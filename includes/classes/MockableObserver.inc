<?php

/**
 * @file
 * Abstract class MockableObserver declaration. This class is used
 * as an observer of the mockable() function.
 */

/**
 * Observer of the mockable module. Subclasses can be added to
 * observe calls to mockable() by adding them in mockable_observer_add(),
 * and removing them in mockable_observer_remove(). You can get all
 * observer objects in mockable_observer_get().
 */
abstract class MockableObserver {

  /**
   * Inform an observer of some data
   *
   * Called by mockable() for each observer.
   *
   * @param $info
   *   information of which to inform the observer.
   */
  function inform($info) {
    if ($this->getActiveState()) {
      return $this->_inform_($info);
    }
  }

  /**
   * Equivalent abstract function of MockableObserver::inform().
   *
   * Subclasses will need to provide logic here of what needs to be
   * done when they are informed of a call.
   */
  abstract function _inform_($info);

  /**
   * Returns the unlocalized title of this observer.
   */
  function getUnlocalizedTitle() {
    return get_class($this);
  }

  /**
   * Returns the arguments to pass to t() when localizing the title.
   */
  function getUnlocalizedTitleArgs() {
    return array();
  }

  /**
   * Returns the unlocalized description of this observer.
   */
  function getUnlocalizedDesc() {
    return get_class($this);
  }

  /**
   * Returns the arguments to pass to t() when localizing the description.
   */
  function getUnlocalizedDescArgs() {
    return array();
  }

  /**
   * Returns the localized title of this observer.
   */
  function getLocalizedTitle() {
    return t($this->getUnlocalizedTitle(), $this->getUnlocalizedTitleArgs());
  }

  /**
   * Returns the localized description of this observer.
   */
  function getLocalizedDescription() {
    return t($this->getUnlocalizedDesc(), $this->getUnlocalizedDescArgs());
  }

  /**
   * The current active state of this observer.
   * If an observer is active, it will store information about what it is
   * observing.
   * Use the getter and setter, below, to access.
   */
  private $active;

  /**
   * Sets an observer to be active or inactive.
   * If an observer is active, it will store information about what it is
   * observing.
   *
   * @param $state
   *   TRUE if you want to activate this observer, FALSE if you want to
   *   deactivate it.
   */
  function setActiveState($state) {
    $this->active = $state;
  }

  /**
   * Returns the active state of this observer.
   * If an observer is active, it will store information about what it is
   * observing.
   *
   * @return
   *   A boolean value, either TRUE or FALSE.
   */
  function getActiveState() {
    return $this->active;
  }
}
