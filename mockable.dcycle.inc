<?php

/**
 * @file
 * Dcycle continuous integration for mockable
 * See https://drupal.org/sandbox/alberto56/1974172
 */

/**
 * Implements hook_dcycle_test().
 */
function mockable_dcycle_test() {
  return array(
    'dependencies' => array(
      'coder_review' => array(),
    ),
    'policies' => array(
      'dcycle' => array(
        'no-php-module' => array(),
        'readme-in-every-directory' => array(),
      ),
    ),
    'commands' => array(
      'Run Code Review' => array(
        'commands' => array(
          'drush coder-review --minor --comment mockable mockable_browser mockable_browser_js_example mockable_crm_client_example mockable_time_example mockable_time mockable_http_request',
        ),
        'fail' => array(
          'grep' => array(
            '/[1-9] normal warnings/',
            '/[1-9][0-9]* normal warnings/',
            '/[1-9] minor warnings/',
            '/[1-9][0-9]* minor warnings/',
          ),
        ),
      ),
      'Clear the cache so all new Simpletests are registered' => array(
        'commands' => array(
          'drush cc all',
        ),
        'expect-no-response' => TRUE,
      ),
      'Run Simpletests' => array(
        'commands' => array(
          'drush test-run Mockable',
        ),
        'fail' => array(
          'grep' => array(
            '/[1-9] fails/',
            '/[1-9][0-9]* fails/',
            '/[1-9] exceptions/',
            '/[1-9][0-9]* exceptions/',
          ),
        ),
      ),
    ),
  );
}
