// see http://net.tutsplus.com/tutorials/javascript-ajax/testing-javascript-with-phantomjs/

var page = require('webpage').create();

var args = require('system').args;
if (args.length === 1) {
    console.log('Missing argument url (example: http://google.com/)');
}

page.open(args[1], function (s) {
  console.log(page.content);
  phantom.exit();
});
