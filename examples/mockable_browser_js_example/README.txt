Mockable Browser Detection Javascript Example
=============================================

This module detects the current visitor's browser on the server-side via PHP and on the client-side via Javascript.

These are displayed on the page mockable_javascript_browser, for example at http://localhost/mockable_javascript_browser.

The purpose of this module is to demonstrate the use of the Mockable Browser (mockable_browser) module, also part of this package.

Note that, in the .info file, mockable_browser is a dependency, but not mockable itself, the reasoning being that, on production sites, you might not want to have the possibility of mocking data.