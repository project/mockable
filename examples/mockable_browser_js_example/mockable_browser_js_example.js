(function ($) {

Drupal.behaviors.mockable_browse_js_example = {
  attach: function (context) {
    // Because of Javascript prototypes, we don't need to change our javascript
    // code to take advantage of mocking.
    $('#userAgent').html(navigator.userAgent);
    $('#appName').html(navigator.appName);
  }
};

})(jQuery);
