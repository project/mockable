Mockable CRM Server Example
===========================

This module acts as a very simple CRM server for demonstration purposes. It is meant to interact with the equally simple Mockable CRM Client Example (mockable_crm_client_example) module, either on the same Drupal site, or on separate sites.

How it works
------------

 * Note that this module is not meant to be used in a real setting, and therefore it is not secure. It is simply a demonstration of how Mockable CRM Client Example (mockable_crm_client_example) can, through the magic of mock objects, be developed and tested without actually having access to Mockable CRM Server Example (mockable_crm_server_example).

 * Make sure you have an account with 'administer users' permission on the server.

 * Use Mockable CRM Client Example (mockable_crm_client_example) to access the server. It will create POSTs to a page on the server, and receive responses.

 * Examine mockable_crm_server_example.test for more details on how to craft requests, and to interpret responses.