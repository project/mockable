Mockable CRM Client Example
===========================

Let's say your module needs to interact via a simplified [REST](http://en.wikipedia.org/wiki/Representational_state_transfer) interace with the included Mockable CRM Server Example, either on this Drupal site or another site, you quickly run into the following problems:

 * You are hitting the external system during your development and tests, and must avoid real transactions. Often, this is remedied by the external system providing a "test" or sandbox version on a different address.

 * The interface between the client and the server is in effect controlled by the server. If the server changes its interface and the client breaks, it is often difficult to know when this happened.

 * It is hard to test how the client reacts to unexpected behaviour of the server (for example long response times or a 500 code), events which are hard to predict.
