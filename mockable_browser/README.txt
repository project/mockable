Mockable Browser
================

Making this module a dependency of your module allows you to use the following
functions in your code

    // php
    $agent = mockable_browser()->userAgent();

    // javascript
    $agent = mockable_browser().userAgent;
    $app = mockable_browser().appName;

Instead of

    // php
    $agent = $_SERVER['HTTP_USER_AGENT'];

    // javascript
    $agent = navigator.userAgent;
    $app = navigator.appName;

The advantage being that you can then use mocking to simulate any user agent you want.

To use mocking, visit admin/config/development/mockable, turn on mock objects and select a browser from the list.

This module does not provide any other user interface and is meant for module developers. For an example of how to use it, please see the Mockable Browser Detection Javascript Example (mockable_browser_js_example) module, part of this package.