(function ($) {

Drupal.behaviors.mockable_browser = {
  attach: function (context) {
    // Only mock the navigator if mocking has been set
    // See http://stackoverflow.com/questions/1307013
    // The following solution works in Safari
    var __originalNavigator = navigator;
    navigator = new Object();
    navigator.__proto__ = __originalNavigator;
    navigator.__defineGetter__('appName', function() {
      return Drupal.settings.mockable_browser.appName;
    });
    navigator.__defineGetter__('userAgent', function() {
      return Drupal.settings.mockable_browser.userAgent;
    });
  }
};

})(jQuery);