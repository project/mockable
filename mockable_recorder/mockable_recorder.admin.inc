<?php

/**
 * @file
 * Administrative tasks.
 */

/**
 * Page callback for recording results
 */
function mockable_recorder_results($form, &$form_state) {
  $calls = variable_get('mockable_observe_calls', array());

  if (!count($calls)) {
    drupal_set_message(t('No calls have recently been made to mockable(). To understand how this works, enable the Mockable Time (mockable_time) module, visit !page, and revisit this page after.', array('!page' => l(t('Time Example'), 'mockable_time_example'))), 'status', FALSE);
    return array();
  }

  $displayers = array();

  drupal_alter('mockable_recorder', $displayers); 
  
  $form = array();

  $form['delete'] = array(
    '#type' => 'checkbox',
    '#title' => t('Delete all @count records', array('@count' => count($calls))),
    '#description' => t('Remove all previously recorded calls.'),
    '#default_value' => FALSE,
  );

  $form['displayer'] = array(
    '#type' => 'select',
    '#title' => t('Display results'),
    '#description' => t('Select a format to display the results'),
    '#options' => array(
      '0' => t('don\'t display')
    ),
  );

  foreach ($displayers as $displayer => $displayer_info) {
    module_load_include($displayer_info['ext'], $displayer_info['module'], $displayer_info['file']);
    if (class_exists($displayer)) {
      $displayer_object = new $displayer;
      $form['displayer']['#options'][$displayer] =   $displayer_object->getLocalizedName();
    }
  }
  $form['displayer']['#default_value'] = 0;

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  if (isset($form_state['input']['displayer']) && $form_state['input']['displayer'] && class_exists($form_state['input']['displayer']) && key_exists($form_state['input']['displayer'], $form['displayer']['#options'])) {
    $displayer = new $form_state['input']['displayer'];
    $form['display'] = array(
      '#type' => 'markup',
      '#markup' => $displayer->display($calls),
    );
    $form['displayer']['#default_value'] = $form_state['input']['displayer'];
  }


  return $form;
}

/**
 * Submit handler for mockable_recorder_results().
 */
function mockable_recorder_results_submit($form, &$form_state) {

  if (isset($form_state['input']['delete']) && $form_state['input']['delete']) {
    mockable_recorder_delete_all();
    drupal_set_message(t('All previously-recorded items have been deleted.'));
  }

  // The following line allows us to display data in the form callback.
  // See https://drupal.org/node/2064545
  $form_state['rebuild'] = TRUE;
}
