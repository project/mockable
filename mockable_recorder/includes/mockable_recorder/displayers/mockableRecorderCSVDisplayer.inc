<?php

/**
 * @file
 * Displays recorded mockable calls in a CSV format.
 */

/**
 * Displays recorded mockable calls in a CSV format.
 */
class mockableRecorderCSVDisplayer extends MockableRecorderDisplayer {

  /**
   * Overrides MockableRecorderDisplayer::getLocalizedName().
   */
  function getLocalizedName() {
    return t('Comma-separated-value (CSV) downloader');
  }

  /**
   * Overrides MockableRecorderDisplayer::display().
   */
  function display($calls) {
    // This code adapted from a Google-cached version of
    // http://zerotodrupal.com/content/outputting-csv-data-file-download
    // Add the headers needed to let the browser know this is a csv file download.
    drupal_add_http_header('Content-Type', 'text/csv; utf-8');
    drupal_add_http_header('Content-Disposition', 'attachment; filename = nodes.csv');

    // Instead of writing to a file, we write to the output stream.
    $fh = fopen('php://output', 'w');

    // We have an array of items, each of which can be an array. We need
    // to transform it into a flat array.
    $csv = $this->toFlat($calls);

    // Add a header row
    fputcsv($fh, $csv['header']);

    // Loop through our nodes and write the csv data.
    foreach($csv['rows'] as $row) {
      fputcsv($fh, $row);
    }

    // Close the output stream.
    fclose($fh);
    die();
  }

  /**
   * Retrieves the displayed items as a flat CSV.
   */
  function toFlat($calls) {
    return self::_toFlat_($calls);
  }

  /**
   * Static equivalent of mockableRecorderCSVDisplayer::toFlat().
   */
  static function _toFlat_($calls) {
    $return = array(
      'header' = array(),
      'rows' => array(),
    );

    // First, create the header.
    foreach ($calls as $call) {
      foreach ($call as $column => $info) {
        if ($column == 'args') {
          count($info)
        }
        if (!is_array($info) && !in_array($column, $return['header'])) {
        }
      }
    }
  }
}
