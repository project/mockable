<?php

/**
 * @file
 * Displays recorded mockable calls in a dpm() format if the devel module
 * exists.
 */

/**
 * Displays recorded mockable calls in a dpm() format if the devel module
 * exists.
 */
class mockableRecorderDpmDisplayer extends MockableRecorderDisplayer {
  /**
   * Overrides MockableRecorderDisplayer::getLocalizedName().
   */
  function getLocalizedName() {
    return t('DPM (dpm()) displayer, available if the devel module is on');
  }

  /**
   * Overrides MockableRecorderDisplayer::display().
   */
  function display($calls) {
    static $called = FALSE;
    if (module_exists('devel') && !$called) {
      dpm($calls);
      $called = TRUE;
    }
  }
}
