<?php

/**
 * @file
 * Displays recorded mockable calls in a print_r format.
 */

/**
 * Displays recorded mockable calls in a print_r format.
 */
class mockableRecorderPrintrDisplayer extends MockableRecorderDisplayer {
  /**
   * Overrides MockableRecorderDisplayer::getLocalizedName().
   */
  function getLocalizedName() {
    return t('print_r() displayer');
  }

  /**
   * Overrides MockableRecorderDisplayer::display().
   */
  function display($calls) {
    return '<pre>' . print_r($calls, TRUE) . '</pre>';
  }
}
