The mockable test module is used only by Mockable's automated simpletests, and is not meant to be used as a basis for your projects.

This module is used to test that very basic Mockable functionality actually works.
