(function ($) {

Drupal.behaviors.mockable_browser = {
  attach: function (context) {
    function setTime() {
      // See http://stackoverflow.com/questions/6312993/javascript-seconds-to-time-with-format-hhmmss
      Drupal.settings.mockable_time.speed = $('#edit-mockable-time-javascript').val() * 10;
      var myDate = new Date();
      var myTime = myDate.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1") + '.' + myDate.getMilliseconds();
      $('.mockable-form-example-time span').html(myTime);
      $('.mockable-form-example-time-show a').click(function() {
        $('.mockable-form-example-time').show();
        $('.mockable-form-example-time-show').hide();
      });
      $('.mockable-form-example-time a').click(function() {
        $('.mockable-form-example-time').hide();
        $('.mockable-form-example-time-show').show();
      });
    };
    setTime();
    // In a production setting, setting this to 1000 is OK, because the time
    // will change every second. However if we want to speed up the clock with
    // the Mockable module, we can simulate speeds of, for example, 30 times the
    // real time, in which case we need to set a faster interval.
    var timer = setInterval(setTime, 30);
  }
};

})(jQuery);