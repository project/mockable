(function ($) {
  // Putting this in a Drupal Behaviour causes a small delay before the mock
  // date kicks in.

  // because we want to set the time to its mock value (for example 1:30:00)
  // when the page is first called, but we will be simulating the passage of
  // time. So, if you leave your page open, every second, the time will
  // update to 1:30:01, 1:30:02, etc., even though the mock time is always set
  // to 1:30:00.
  var callTime = new Date().getTime();
  var oldDate = Date;
  Date = function (fake)
  {
    // see http://stackoverflow.com/questions/847185/convert-a-unix-timestamp-to-time-in-javascript
    var curTime = new oldDate().getTime();
    return new oldDate((Drupal.settings.mockable_time.time * 1000) + (curTime - callTime) * Drupal.settings.mockable_time.speed / 1000);
  }
})(jQuery);
