Mockable HTTP Request
=====================

Allows you to mock interactions with third-party systems via HTTP.

Set up mock objects for HTTP requests in your modules
-----------------------------------------------------

The following instructions are meant to help you set up mock objects for
your module which uses drupal_http_request.

* Start by adding mockable_http_request as a dependency of your module.

* Replace all instances of drupal_http_request() by mockable_http_request(). Everything should work as before.

* You can now enable Mockable, go to admin/config/development/mockable and set up using mock objects, specifying that you would like to mock a timeout, the wrong data being returned, etc.

* Still, you must set up your module to mock normal usage. This can be done by hijacking real HTTP requests to build a CSV file. Here is how.

* If possible (if the real external system is up and running), use your module for various tasks.

* Go to 
